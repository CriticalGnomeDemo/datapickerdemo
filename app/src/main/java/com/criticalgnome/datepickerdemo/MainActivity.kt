package com.criticalgnome.datepickerdemo

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DatePickerFragment().display(
            manager = supportFragmentManager,
            tag = DIALOG_TAG,
            calendar = Calendar.getInstance().apply { set(1976, 0, 5) },
            callback = object : DatePickerFragment.Callback {
                override fun onDateSelected(calendar: Calendar) {
                    Toast.makeText(this@MainActivity, SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.US).format(calendar.time), Toast.LENGTH_SHORT).show()
                }
            })
    }

    companion object {
        private const val DIALOG_TAG = "DatePicker"
        private const val DATE_FORMAT_PATTERN = "EEE, MMM d, YYYY"
    }
}
